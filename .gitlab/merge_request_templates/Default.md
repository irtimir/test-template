### Checklist

- [ ] clean code, documentation if not
- [ ] unit tests
- [ ] linters

do not criticize people, but criticize the code
